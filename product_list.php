<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Product list</title>
    <link rel="stylesheet" href="main.css">
</head>
<body>
    <header>
        <h1>Product list</h1>
            <div class="nav-btn">
                <input type="button"  id="#delete-product-btn" value="MASS DELETE"></input>
                <a href="product_add.html"><input type="button" class="" value="ADD"></input></a>
            </div>
    </header>
    <table>
    <tr>
        <td>
            <input type="checkbox" class=".delete-checkbox">
            <div class="name" id="#name">
                <?php
                $name1="ACME disk";
                echo $name1;
                ?>
            </div>
            <div class="sku" id="#sku">
                <?php
                    $sku1=123456;
                    echo "SKU:",$sku1;
                ?>
            </div>
            <div class="price" id="#price">
                <?php
                    $price1=15;
                    echo "Price:", $price1,"$";
                ?>
            </div>
            <div class="attributes" id="#size">
                <?php
                    $attribute1=700;
                    echo "size:",$attribute1, "MB";
                ?>
            </div>
        </td>
        <td>
            <input type="checkbox" class=".delete-checkbox">
            <div class="name" id="#name">
                <?php
                $name2="ACME disk";
                echo $name2;
                ?>
            </div>
            <div class="sku" id="#sku">
                <?php
                    $sku2=123444;
                    echo "SKU:",$sku2;
                ?>
            </div>
            <div class="price" id="#price">
                <?php
                    $price2=15;
                    echo "Price:", $price2,"$";
                ?>
            </div>
            <div class="attributes" id="#size">
                <?php
                    $attribute2=700;
                    echo "size:",$attribute2, "MB";
                ?>
            </div>
        </td>
        <td>
            <input type="checkbox" class=".delete-checkbox">
            <div class="name" id="#name">
                <?php
                $name3="ACME disk";
                echo $name3;
                ?>
            </div>
            <div class="sku" id="#sku">
                <?php
                    $sku3=123455;
                    echo "SKU:",$sku3;
                ?>
            </div>
            <div class="price" id="#price">
                <?php
                    $price3=15;
                    echo "Price:", $price3,"$";
                ?>
            </div>
            <div class="attributes" id="#size">
                <?php
                    $attribute3=700;
                    echo "size:",$attribute3, "MB";
                ?>
            </div>
        </td>
        <td>
            <input type="checkbox" class=".delete-checkbox">
            <div class="name" id="#name">
                <?php
                $name4="ACME disk";
                echo $name4;
                ?>
            </div>
            <div class="sku" id="#sku">
                <?php
                    $sku4=123696;
                    echo "SKU:",$sku4;
                ?>
            </div>
            <div class="price" id="#price">
                <?php
                    $price4=15;
                    echo "Price:", $price4,"$";
                ?>
            </div>
            <div class="attributes" id="#size">
                <?php
                    $attribute4=700;
                    echo "size:",$attribute4, "MB";
                ?>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <input type="checkbox" class=".delete-checkbox">
            <div class="name" id="#name">
                <?php
                $name5="War and Peace";
                echo $name5;
                ?>
            </div>
            <div class="sku" id="#sku">
                <?php
                    $sku5=162332;
                    echo "SKU:",$sku5;
                ?>
            </div>
            <div class="price" id="#price">
                <?php
                    $price5=20;
                    echo "Price:", $price5,"$";
                ?>
            </div>
            <div class="attributes" id="#weight">
                <?php
                    $attribute5=1;
                    echo "Weight:",$attribute5, "KG";
                ?>
            </div>
        </td>
        <td>
            <input type="checkbox" class=".delete-checkbox">
            <div class="name" id="#name">
                <?php
                $name6="War and Peace";
                echo $name6;
                ?>
            </div>
            <div class="sku" id="#sku">
                <?php
                    $sku6=166666;
                    echo "SKU:",$sku6;
                ?>
            </div>
            <div class="price" id="#price">
                <?php
                    $price6=20;
                    echo "Price:", $price6,"$";
                ?>
            </div>
            <div class="attributes" id="#weight">
                <?php
                    $attribute6=1;
                    echo "Weight:",$attribute6, "KG";
                ?>
            </div>
        </td>
        <td>
            <input type="checkbox" class=".delete-checkbox">
            <div class="name" id="#name">
                <?php
                $name7="War and Peace";
                echo $name7;
                ?>
            </div>
            <div class="sku" id="#sku">
                <?php
                    $sku7=16233;
                    echo "SKU:",$sku7;
                ?>
            </div>
            <div class="price" id="#price">
                <?php
                    $price7=20;
                    echo "Price:", $price7,"$";
                ?>
            </div>
            <div class="attributes" id="#weight">
                <?php
                    $attribute7=1;
                    echo "Weight:",$attribute7, "KG";
                ?>
            </div>
        </td>
        <td>
            <input type="checkbox" class=".delete-checkbox">
            <div class="name" id="#name">
                <?php
                $name8="War and Peace";
                echo $name8;
                ?>
            </div>
            <div class="sku" id="#sku">
                <?php
                    $sku8=16123;
                    echo "SKU:",$sku8;
                ?>
            </div>
            <div class="price" id="#price">
                <?php
                    $price8=20;
                    echo "Price:", $price8,"$";
                ?>
            </div>
            <div class="attributes" id="#weight">
                <?php
                    $attribute8=1;
                    echo "Weight:",$attribute8, "KG";
                ?>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <input type="checkbox" class=".delete-checkbox">
            <div class="name" id="#name">
                <?php
                $name9="Chair";
                echo $name9;
                ?>
            </div>
            <div class="sku" id="#sku">
                <?php
                    $sku9=999123;
                    echo "SKU:",$sku9;
                ?>
            </div>
            <div class="price" id="#price">
                <?php
                    $price9=25;
                    echo "Price:", $price9,"$";
                ?>
            </div>
            <div class="attributes" id="#lenght" id="width" id="height">
                <?php
                    $attribute9_1=30;
                    $attribute9_2=50;
                    $attribute9_3=70;
                    echo $attribute9_1 ,"x", $attribute9_2,"x",$attribute9_3, "CM";
                ?>
            </div>
        </td>
        <td>
            <input type="checkbox" class=".delete-checkbox">
            <div class="name" id="#name">
                <?php
                $name10="Chair";
                echo $name10;
                ?>
            </div>
            <div class="sku" id="#sku">
                <?php
                    $sku10=999222;
                    echo "SKU:",$sku10;
                ?>
            </div>
            <div class="price" id="#price">
                <?php
                    $price10=25;
                    echo "Price:", $price10,"$";
                ?>
            </div>
            <div class="attributes" id="#lenght" id="width" id="height">
                <?php
                    $attribute10_1=30;
                    $attribute10_2=50;
                    $attribute10_3=70;
                    echo $attribute10_1 ,"x", $attribute10_2,"x",$attribute10_3, "CM";
                ?>
            </div>
        </td>
        <td>
            <input type="checkbox" class=".delete-checkbox">
            <div class="name" id="#name">
                <?php
                $name11="Chair";
                echo $name11;
                ?>
            </div>
            <div class="sku" id="#sku">
                <?php
                    $sku11=999333;
                    echo "SKU:",$sku11;
                ?>
            </div>
            <div class="price" id="#price">
                <?php
                    $price11=25;
                    echo "Price:", $price11,"$";
                ?>
            </div>
            <div class="attributes" id="#lenght" id="width" id="height">
                <?php
                    $attribute11_1=30;
                    $attribute11_2=50;
                    $attribute11_3=70;
                    echo $attribute11_1 ,"x", $attribute11_2,"x",$attribute11_3, "CM";
                ?>
            </div>
        </td>
        <td>
            <input type="checkbox" class=".delete-checkbox">
            <div class="name" id="#name">
                <?php
                $name12="Chair";
                echo $name12;
                ?>
            </div>
            <div class="sku" id="#sku">
                <?php
                    $sku12=999666;
                    echo "SKU:",$sku12;
                ?>
            </div>
            <div class="price" id="#price">
                <?php
                    $price12=25;
                    echo "Price:", $price12,"$";
                ?>
            </div>
            <div class="attributes" id="#lenght" id="width" id="height">
                <?php
                    $attribute12_1=30;
                    $attribute12_2=50;
                    $attribute12_3=70;
                    echo $attribute12_1 ,"x", $attribute12_2,"x",$attribute12_3, "CM";
                ?>
            </div>
        </td>
    </tr>
    </table>
</body>
</html>